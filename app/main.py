# -*- coding: utf-8 -*-
import asyncio
import logging
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from pkg import conf
from pkg.util import core, stream
from pkg.brain import random
from pkg.mysql import core as mql_core, sql

logging.basicConfig(filename=core.get_log(), level=logging.INFO)

bot = Bot(token=conf.token)
dp = Dispatcher(bot)


async def handle_message(message):
    # получаем случайный ответ
    answer = core.get_random(random.answer)

    # отсылаем ответ пользователю
    await bot.send_message(message.chat.id, answer)

    # сохраняем диалог в базу данных
    stream.save(message, answer)


@dp.message_handler()
async def echo_message(message: types.Message):
    try:
        await handle_message(message)
    except Exception as ex:
        logging.error('Error at %s', 'echo_message', exc_info=ex)
        await bot.send_message(message.chat.id, conf.oops)


if __name__ == '__main__':
    tbl = mql_core.check_table('stream')
    if not tbl:
        mql_core.query(sql.create_table_stream)
    executor.start_polling(dp)
