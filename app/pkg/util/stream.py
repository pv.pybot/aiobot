# -*- coding: utf-8 -*-
import logging
from pkg.mysql import tbl_stream
from pkg.util import core

logging.basicConfig(filename=core.get_log(), level=logging.INFO)


def save(message, answer):
    """
    Вставить запись в таблицу 'stream'
    :param message: object
    :param answer: string
    :return: int
    """
    if message:
        try:
            content = message.text

            id_mess = message.message_id
            id_chat = message.chat.id

            list_rows = [
                [
                    content,
                    answer,
                    id_mess,
                    id_chat,
                    message.from_user.first_name,
                    message.from_user.last_name,
                    message.from_user.username,
                ]
            ]

            return tbl_stream.insert(list_rows)
        except Exception as exception:
            logging.error('Error at %s', 'pkg/util/stream/save', exc_info=exception)
