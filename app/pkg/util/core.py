# -*- coding: utf-8 -*-
import datetime
import os
import random


def get_random(some_list):
    """
    Выбрать случайное значение из списка.
    Если аргумент не список - стандартный ответ
    :param some_list: list
    :return: string
    """
    result = 'Даже не знаю, что ответить...'
    if isinstance(some_list, list):
        result = random.choice(some_list)
    return result


def get_date_now():
    """
    Получить текущее время: 11.11.2020 11:00
    :return: string
    """
    now = datetime.datetime.now()
    return now.strftime("%d.%m.%Y %H:%M")


def get_log(log_file='log/bot.log'):
    """
    Получить путь к лог-файлу
    :param log_file: string
    :return: string
    """
    return get_file(log_file)


def get_file(path):
    """
    Получить путь к файлу относительно корня
    :param path: string
    :return: string
    """
    root = os.getcwd()
    return '/'.join([root, path])
