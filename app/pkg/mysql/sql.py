# -*- coding: utf-8 -*-
create_table_stream = '''create table stream
(
    id              int auto_increment primary key,
    message         varchar(256)                       null,
    answer          varchar(256)                       null,
    id_mess         int                                null,
    id_chat         int                                null,
    user_name       varchar(64)                       null,
    user_surname    varchar(64)                       null,
    user_nickname   varchar(64)                       null,
    date_time       datetime default CURRENT_TIMESTAMP null
);'''