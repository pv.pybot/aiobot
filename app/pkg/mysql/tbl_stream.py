# -*- coding: utf-8 -*-
from pkg.mysql import core
import logging
from pkg.util import core as core_util

table = 'stream'

logging.basicConfig(filename=core_util.get_log(), level=logging.INFO)


def insert(list_rows):
    try:
        for row in list_rows:
            if isinstance(row, list):
                sprintf = ''
                data = tuple(row)
                for number in range(len(row)):
                    sprintf += '%s, '
                sprintf = sprintf.strip(', ')
                sprintf = 'null, %s, now()' % sprintf
                return core.insert(table, sprintf, data)
    except Exception as ex:
        logging.error('Error at %s', 'tbl_unknown.insert', exc_info=ex)


def select_all(where=None):
    return core.select_all(table, where)


def select(where=None, fetch='all'):
    return core.select(table, where, fetch)


def update(set_condition, where):
    return core.update(table, set_condition, where)


def delete(where):
    return core.delete(table, where)
