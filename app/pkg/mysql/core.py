# -*- coding: utf-8 -*-
import logging
from pkg import conf
from pkg.util import core
import pymysql

logging.basicConfig(filename=core.get_log(), level=logging.INFO)


def select_one(table, where=None):
    """
    Выбрать одну запись по условию
    Используется fetchone()
    :param table: string
    :param where: string
    :return: tuple
    """
    try:
        con = conn()
        if con:
            sql = 'select * from `%s`' % table
            if where:
                sql = '%s where %s' % (sql, where)
            cur = con.cursor()
            cur.execute(sql)
            return cur.fetchone()
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/select_one', exc_info=ex)
        return None


def select_all(table, where=None):
    """
    Выбрать все записи по условию
    Используетсы: fetchall()
    :param table: string
    :param where: string
    :return: tuple
    """
    try:
        con = conn()
        if con:
            sql = 'select * from `%s`' % table
            if where:
                sql = '%s where %s' % (sql, where)
            cur = con.cursor()
            cur.execute(sql)
            return cur.fetchall()
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/select_all', exc_info=ex)
        return None


def select(table, where=None, fetch='all'):
    """
    Выполнить запрос типа INSERT, UPDATE, DELETE
    :param table: string
    :param where: string
    :param fetch: string
    :return: tuple
    """
    try:
        con = conn()
        if con:
            sql = 'select * from `%s`' % table
            if where:
                sql = '%s where %s' % (sql, where)
            cur = con.cursor()
            cur.execute(sql)
            if fetch == 'all':
                return cur.fetchall()
            elif fetch == 'one':
                return cur.fetchone()
            else:
                raise Exception('Unknown fetch applied: "%s"' % fetch)
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/select', exc_info=ex)
        return None


def insert(table, sprintf_values, data):
    """
    Вставить в таблицу
    :param table: string
    :param sprintf_values: string
    :param data: list
    :return:
    """
    try:
        sql = "insert into `%s` values (%s)" % (table, sprintf_values)
        return query(sql, data)
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/insert', exc_info=ex)


def update(table, set_condition, where):
    """
    Обновить запись по условию
    :param table: string
    :param set_condition: string
    :param where: string
    :return:
    """
    try:
        sql = 'update `%s` set %s where %s' % (table, set_condition, where)
        return query(sql)
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/update', exc_info=ex)


def delete(table, where):
    """
    Удалить из таблицы по условию
    :param table: string
    :param where: string
    :return:
    """
    try:
        sql = 'delete from %s where %s' % (table, where)
        return query(sql)
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/delete', exc_info=ex)


def query(sql, data=None):
    """
    Выполнить запрос типа INSERT, UPDATE, DELETE, CREATE, etc
    :param sql: string
    :param data: list
    :return:
    """
    try:
        con = conn()
        if con:
            cur = con.cursor()
            if data:
                cur.execute(sql, data)
            else:
                cur.execute(sql)
            return cur.rowcount
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/query', exc_info=ex)


def check_table(table_name):
    """
    Проверить наличие таблицы в базе данных
    :param table_name: string
    :return: int
    """
    try:
        mydb = aiodb()
        cur = mydb.cursor()
        sql = "SHOW TABLES LIKE '%s'" % table_name
        cur.execute(sql)
        rows = cur.fetchall()
        return len(rows)
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/check_table', exc_info=ex)


def aiodb():
    """
    Соединиться с базой данных 'aiobot'
    :return: pymysql.connect
    """
    try:
        return conn(conf.db_name)
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/aiodb', exc_info=ex)


def conn(db_name=conf.db_name):
    """
    Соединение с базой данных
    :param db_name: string
    :return: pymysql.connect
    """
    try:
        return pymysql.connect(
            host=conf.db_host,
            port=conf.db_port,
            user=conf.db_user,
            password=conf.db_pass,
            database=db_name,
            charset='utf8mb4',
            autocommit=True
        )
    except Exception as ex:
        logging.error('Error at %s', 'mysql/core/conn', exc_info=ex)
        return None
