# установка базового образа (host OS)
FROM python:3.9.0

ARG HOME_DIR=/aiobot

# установка рабочей директории в контейнере
WORKDIR ${HOME_DIR}

# копирование файла зависимостей в рабочую директорию
COPY app/requirements.txt .

# установка зависимостей
RUN pip install -r requirements.txt

# копирование содержимого локальной директории src в рабочую директорию
COPY app/ .

# команда, выполняемая при запуске контейнера
CMD [ "python3", "main.py" ]